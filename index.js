// alert("hi");

/*
    JSON Objects
        - JSON stands for Javascript Object Notation
        - JSON can also be used in othet programming languages
        - Do not confuse Javascript objects with JSON objects
        - JSON is used for serializing different data types into bytes
            - serialization - process of converting data types into series of bytes for easier transmission or transfer of information
            - bytes - are infromation that a computer processes to perform different tasks

        - Uses double quotes for property name
        - Syntax:
            {
                "propertyA" : "valueA"
                "propertyB" : "valueB"
            }

            {
                "name" : "Andrea",
                "birthDay" : "December 17, 1991"
            };
*/

// JSON Objects

    //{
        //"city" : "Malabon City",
        //"province" : "Metro Manila",
        //"country" : "Philippines"
    //}

    // JSON Arrays
    //"cities" : [
        //{
        //"city" : "Malabon City",
        //"province" : "Metro Manila",
        //"country" : "Philippines"
        //},
        //{
        //"city" : "Cebu City",
        //"province" : "Cebu",
        //"country" : "Philippines"
        //}
    //]

    // JSON Methods
        // JSON objects contain methods for parsing and converting data info stringified JSON

    let batchesArr = [
        {batchName: "Batch 169"},
        {batchName: "Batch 170"}
    ];

    console.log(JSON.stringify(batchesArr));

    let data = JSON.stringify({
        name: "Rey",
        age: 28,
        address: {
            city: "Valenzuela",
            country: "Philippines"
        }
    });
        console.log(data);

    // Using Stringify method with variables

    // User details

    let firstName = prompt("First Name:");
    let lastName = prompt("Last Name:");
    let age = prompt("Age");
    let address = {
        city: prompt("City:"),
        country: prompt("Country:")
    };

    let data2 = JSON.stringify({
        firstName : firstName,
        lastName : lastName,
        age : age,
        address : address
    });
        console.log(data2);

    // Converting Stringified objects to Javascript Objects

    let batchesJSON = `[
        {
            "batchName" : "Batch 169"
        },
        {
            "batchName" : "Batch 170"
        }
    ]`
        console.log(JSON.parse(batchesJSON));